# NOMAD Mini-Apps Suite

The NOMAD Mini-Apps Suite is a comprehensive collection of mini-applications designed for benchmarking and co-design purposes. These mini-apps are composed of isolated, computationally intensive, and relevant kernels from the ABINIT, exciting, FHI-aims and ELPA codes. This README file provides instructions for compiling the suite using CMake and running the benchmarks, as well as a list of tested dependencies for different environments.

More details about the suite are given in the following publication:
Mas Magre I, Grima Torres R, Cela Espín JM and Gutiérrez Moreno J. The NOMAD mini-apps: A suite of kernels from ab initio electronic structure codes enabling co-design in high-performance computing. Open Reseaerch Europe 2024, 4:35 (https://doi.org/10.12688/openreseurope.16920.2)

## About the mini-apps

### ABINIT mini-app

The ABINIT mini-app, abinit-chi0, reproduces the kernel identified in the chi0 subroutine within the source code chi0.F90. This mini-app starts by reading a checkpoint file containing input parameters and initializing the required variables. It then iterates over k-points and bands to compute the rho-twiddle function and assembles the linear density response function, χ0. For more information, please refer to [src/abinit-chi0/README.md](src/abinit-chi0/README.md).

### exciting mini-app

The exciting mini-app, exciting-calcselfc, performs a single iteration of a loop in a GW cycle to calculate quasiparticle energies. It begins by reading a checkpoint file containing input parameters, proceeds to allocate memory for the M matrix, expands the products using the expand_products subroutine, and calculates the weight of the M matrix using the calcmwm subroutine. For more information, please refer to [src/exciting-calcselfc/README.md](src/exciting-calcselfc/README.md).

### FHI-aims mini-app

The FHI-aims mini-app, fhiaims-lvl-tricoeff, computes the LVL triple expansion coefficients in real space (i.e., for a set of Bravais vectors which separate the two unit cells where the basis pairs live) and Fourier transfrom the LVL triple coefficients from real space (on a Bravais lattice) to reciprocal space.

### ELPA mini-app

The ELPA mini-app, elpa-tridi2band, reproduces the fourth computational step of the two-stage tridiagonalization method. It converts the tridiagonal matrix to a band matrix. Although ELPA is technically a math library, it is a relevant component to many computational chemistry codes. For more information, please refer to [src/elpa-tridi2band/README.md](src/elpa-tridi2band/README.md).

## Compilation using CMake

To compile the NOMAD Mini-Apps Suite, please follow these steps:

1. Clone the repository to your local machine.

2. Ensure that the following dependencies are installed:

   - CMake (version >= 3.5)
   - MPI
   - BLAS

4. Run CMake to generate and configure the build files:
   ```shell
   cmake -B <build_dir>
   ```

5. Build the mini-apps:
   ```shell
   make -C  <build_dir> # Use -DOPTIMIZE_FOR_NATIVE=ON for a more agresive optimization
   ```

Once the compilation process is successfully completed, you will find the executables for the mini-apps in the `build/bin/` directory.

#### Building for multiple toolchains

You can edit file `machines` in order to set your available toolchains and the characteristics of your computer. You have to set at least one mode, specifying the required modules for that specific toolchain. Next, you have to specify some of the characteristics of your machine: number of threads per core, number of cores and number of sockets. Finally, if necessary, you can set specific configuration flags for your queue system, like partition, account, qos, hint, etc. You can see some examples inside the file.

```shell
[[ "${HOSTNAME}" == "<your-host-name>"* ]] && {
    name="<any-name>"
    declare -A modes=( [gnu]="<modules-required gnu+MPI+BLAS>"
                       [intel]="<modules-required Intel+MPI+BLAS>"
                       [nvhpc]="<modules-required nvidia+MPI+BLAS>" )
    ThrXcore=2
    NCORES=128
    NSOCKETS=2
    EXTRAQ="#SBATCH --partition <your-partition>"$'\n'
    EXTRAQ+="#SBATCH --account <your-account>"$'\n'
    EXTRAQ+="#SBATCH --mem=0"
}
```

You can build two versions of the miniapp for every toolchain: one with basic optimization an another with a more aggressive optimization:

```bash
./makeall.sh
```



## Running the benchmarks

The `benchmarks/` folder contains subfolders for each mini-app. First of all, you need to download the checkpoints required to run the benchmarks. It can  be downloaded and extracted automatically by running the script `get-ckpts.sh`.

In order to run the different benchmarks you have to fill  `machines` file. Look at the previous [section](#building-for-multiple-toolchains). Set BUILD_DIR environment variable with the route of your compilation and execute `run.sh`.

```shell
BUILD_DIR=../../build ./run.sh
```

Or use the default build dirs defined by `makeall.sh` and run a test for every toolchain.

```shell
./runall.sh
```



## Tested Dependencies (26/04/2024)

The NOMAD Mini-Apps Suite has been tested with the following dependencies in different environments:

### Modules loaded in MareNostrum-5 (BSC, Spain)

- VERSION(INTEL): 

```shell
cmake oneapi
```

- VERSION(GNU): 

```shell
cmake openmpi/4.1.5-gcc mkl
```

### Modules loaded in MareNostrum-4 (BSC, Spain)

- VERSION(INTEL): 
```shell
cmake intel impi mkl
```
- VERSION(GNU): 
```shell
cmake/3.18.3 gcc impi mkl 
```
```shell
cmake/3.18.3 gcc/4.8.5 impi mkl 
```

### Modules loaded in CTE-POWER (Power9 system, BSC, Spain)

- VERSION(GNU): 
```shell
cmake/3.15.4 gcc/7.3.0 openmpi/3.0.0 openblas/0.3.6
```
- VERSION(XLF): 
```shell
cmake/3.15.4 ibm/16.1.1.2 openmpi/4.0.1
```
- VERSION(PGI): 
```shell
cmake/3.15.4 nvidia-hpc-sdk/22.2
```

### Modules loaded in Leonardo (CINECA, Italy)

- VERSION(INTEL):
```shell
intel-oneapi-compilers/2023.0.0 intel-oneapi-mpi/2021.7.1 intel-oneapi-mkl/2022.2.1
```

### Modules loaded in LUMI (Finland)

- VERSION(CRAY): 

```shell
LUMI/23.09 partition/C PrgEnv-cray/8.4.0 buildtools/23.09 cray-libsci/23.09.1.1
```

- VERSION(GNU): 

```shell
LUMI/23.09 partition/C PrgEnv-gnu/8.4.0 buildtools/23.09 cray-libsci/23.09.1.1
```

### Modules loaded in VEGA (IZUM, Slovenia)

- VERSION(GNU): 

```shell
CMake GCC/12.3.0 OpenMPI/4.1.5-GCC-12.3.0 OpenBLAS/0.3.23-GCC-12.3.0
```

- VERSION(INTEL): 

```shell
CMake intel-compilers/2022.2.1  openmpi/intel/4.1.2.1 imkl/2022.2.1
```

- VERSION(NVHPC): 

```shell
CMake NVHPC/22.7 imkl/2022.2.1
```

### Modules loaded in Karolina (IT4I, Czech Republic)

- VERSION(INTEL): 
```shell
CMake intel-compilers impi imkl
```
- VERSION(AMD): 
```shell
# AOCC+OpenMPI => Need an OpenMPI compiled with AOCC!
```

- VERSION(NVIDIA): 
```shell
FC=pgfortran
CC=pgcc
CXX=pgc++
CMake/3.20.1 NVHPC OpenMPI/4.1.2-NVHPC-22.2-CUDA-11.6.0

```

These dependencies have been tested to ensure proper functionality of the NOMAD Mini-Apps Suite in the respective environments. Please make sure to have the required dependencies installed and properly configured before compiling the mini-apps.

Feel free to modify and enhance this README file as needed to include additional information or steps specific to your use case.

# Acknowledgements
This work has been funded and developed within the framework of the NOMAD Centre or Excellence, funded by the European Union’s Horizon 2020 research and innovation program under grant agreement No. 951786.