#ifdef _TRACE_
  subroutine MY_MPI_Recv( buf, count, datatype, source, tag, comm, status, ierror)
  use mpi
  implicit none
  REAL*8 :: buf
  INTEGER, INTENT(IN) :: count, source, tag, datatype, comm
  INTEGER, INTENT(OUT) :: ierror, status(mpi_status_size)
  call MPI_Recv( buf, count, datatype, source, tag, comm, status, ierror)
  end subroutine MY_MPI_Recv


  subroutine MY_MPI_Send( buf, count, datatype, dst, tag, comm, ierror)
  use mpi
  implicit none
  REAL*8 :: buf
  INTEGER, INTENT(IN) :: count, dst, tag, datatype, comm
  INTEGER, INTENT(OUT) :: ierror
  call MPI_Send( buf, count, datatype, dst, tag, comm, ierror)
  end subroutine MY_MPI_Send

  subroutine MY_MPI_WAIT( REQUEST, STATUS, IERROR )
  use mpi
  implicit none
  INTEGER    REQUEST, STATUS(MPI_STATUS_SIZE), IERROR
  call MPI_Wait(REQUEST, STATUS, IERROR)
  end subroutine MY_MPI_WAIT

  subroutine MY_MPI_WAITALL( COUNT, ARRAY_OF_REQUESTS, ARRAY_OF_STATUSES, IERROR )
  use mpi
  implicit none
  INTEGER    COUNT, ARRAY_OF_REQUESTS(*)
  INTEGER    ARRAY_OF_STATUSES(MPI_STATUS_SIZE,*), IERROR
  call MPI_Waitall(COUNT, ARRAY_OF_REQUESTS, ARRAY_OF_STATUSES, IERROR)
  end subroutine MY_MPI_WAITALL

  SUBROUTINE MY_MPI_BCAST( BUF, COUNT, DATATYPE, ROOT, COMM, IERROR )
  use mpi
  implicit none
  REAL*8 :: buf
  INTEGER, INTENT(IN) :: count, datatype, root, comm
  INTEGER, INTENT(OUT) :: ierror
  call MPI_Bcast( buf, count, datatype, root, comm, ierror)
  END SUBROUTINE MY_MPI_BCAST

#define MPI_Recv MY_MPI_Recv
#define MPI_Send MY_MPI_Send
#define MPI_Wait MY_MPI_WAIT
#define MPI_Waitall MY_MPI_WAITALL
#define mpi_bcast MY_MPI_BCAST
#endif
