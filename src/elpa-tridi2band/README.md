# ELPA Mini-app Source Code

This mini-app is developed based on the original ELPA code.

## Licensing and Use
This software is heavily reliant on components from the ELPA library. As such, it adheres to the LGPL v3 license, under which the ELPA codebase is distributed. The full terms and conditions of this license are available in the COPYING folder, which is an integral part of this repository.

For further details on the LGPL v3 license, you can refer to the GNU General Public License website.

## Source Code Origin
The content of this file is a preprocessed version of the original file trans_ev_tridi_to_band_template.F90
that is included inside elpa2_compute.F90 via elpa2_compute_real_template.F90 in the ELPA repository. The original version of these files can be found at <https://gitlab.mpcdf.mpg.de/elpa/elpa/-/tree/master/src/elpa2?ref_type=heads>

## Project Structure and Licensing Compliance
Given the licensing constraints of the ELPA code (LGPL v3), this project is also bound by the same licensing terms. This implies two possible scenarios for the structure and licensing of this mini-app:

Entire Project under LGPL v3: The complete mini-app is distributed under the LGPL v3 license, ensuring full compliance with the licensing terms of the ELPA code.

Segmented Licensing: Alternatively, the project may be segmented into different parts, each governed by its respective license. This approach requires careful separation of the code derived from ELPA (which must remain under LGPL v3) from other parts of the project that might have different licensing terms.

## Acknowledgements
We acknowledge the contribution of the ELPA consortium and the significance of their work in the realm of eigenvalue problem algorithms. Their efforts have been instrumental in the development of this project.

[Link to main README](../../README.md)