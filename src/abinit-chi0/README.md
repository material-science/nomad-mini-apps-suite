# ABINIT Mini-app Source Code

This mini-app is developed based on components from the ABINIT codebase.

## Licensing and Use
This software incorporates elements from the ABINIT package. The majority of this code and associated documents in ABINIT are under the GNU General Public License, Version 3 (GPL v3). A select few routines, notably those related to the NOMAD CoE project (Low-scaling GW and RPA), are licensed under the more liberal APACHE v2.0 license, which is compatible with GPL. The full GPL v3 license can be viewed on the GNU General Public License website.

For details on the APACHE v2.0 license, please visit [https://www.apache.org/licenses/LICENSE-2.0](https://www.apache.org/licenses/LICENSE-2.0).

## Source Code Origin
The contents of this files are adapted from the original ABINIT source code. The ABINIT package, including all its components, can be accessed at <https://www.abinit.org/source-package>. This adaptation has been made to tailor the functionality for specific requirements of this mini-app.

## Project Structure and Licensing Compliance
In accordance with the licensing of the ABINIT package, this project is primarily distributed under the GPL v3 license. However, parts of the project that include or interact with the ABINIT routines under the APACHE v2.0 license need to be checked for compatibility with the overall licensing scheme. This may necessitate a segmented licensing approach:

1. Parts directly derived from or incorporating ABINIT code under GPL v3 will adhere to the same license.
2. Components interfacing with or using ABINIT routines under the APACHE v2.0 license should comply with both GPL v3 and APACHE v2.0 licensing terms.

## Acknowledgements
We gratefully acknowledge the contributions made by the developers of the ABINIT package in developing the kernels included in the mini-app.

[Link to main README](../../README.md)