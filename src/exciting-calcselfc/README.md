# exciting Mini-app Source Code

This mini-app is developed based on the original exciting codebase.

## Licensing and Use
This software is developed by utilizing code from the exciting package. The exciting codebase is distributed under the GNU General Public License, Version 2 (GPL v2), as of June 1991. The full GPL v2 license can be reviewed on the GNU General Public License website. It is important to note that everyone is allowed to copy and distribute verbatim copies of this license, but alterations to the document are not permitted.

## Source Code Origin
The source code of this mini-app is derived from the original exciting source code. This adaptation has been specifically tailored to meet the unique requirements of our project. The original exciting package, along with its source code, is accessible at <https://exciting-code.org>.

## Project Structure and Licensing Compliance
Given that the Exciting code is licensed under GPL v2, our mini-app, which incorporates elements of this code, is also subject to the same licensing conditions. The project thus adheres to the following framework:

- The entirety of the mini-app that includes or is derived from the Exciting code must comply with GPL v2.
- Any new code or modifications made for this mini-app should be compatible with the GPL v2 license to ensure cohesive licensing across the project.

## Acknowledgements
We acknowledge with thanks the significant contributions of the Exciting development team in developing the kernels included in the mini-app.

[Link to main README](../../README.md)