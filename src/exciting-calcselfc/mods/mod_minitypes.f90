! This file is part of exciting, which is distributed under the terms of the GNU General Public License.
! See the file COPYING for license details.
! Copyright (C) Exciting Code, SOL group. 2020

module mod_minitypes
    type frequency
        ! character(40) :: fgrid           ! grid type
        ! character(40) :: fconv           ! real/imaginary frequency
        integer(4)    :: nomeg           ! grid size
        ! real(8)       :: freqmin         ! lower cutoff frequency
        ! real(8)       :: freqmax         ! upper cutoff frequency
        ! real(8), allocatable :: freqs(:) ! frequency grid
        ! real(8), allocatable :: womeg(:) ! integration weights
    end type frequency

    type G_set
    ! grid parameters
    integer, allocatable :: intgv(:,:) ! intgv(3,2) ! integer grid size
    ! G-points
    integer, allocatable :: ivg(:,:)  ! integer coordinates
    integer, allocatable :: ivgig(:,:,:) ! integer coordinates -> 1d index
    end type G_set

    type Gk_set
        ! Reduced (potentially) quantities
        integer, allocatable :: ngk(:,:)      ! number of G+k-vectors for augmented plane waves
        integer, allocatable :: igkig(:,:,:)  ! index from G+k-vectors to G-vectors
        integer, allocatable :: igigk(:,:,:)  ! index from G-vectors to G+k-vectors

    end type Gk_set

    type kq_set
        ! k-points
        integer :: nkpt                     ! number of k/q-points
        real(8), allocatable :: vkl(:,:)    ! lattice coordinates
        real(8), allocatable :: vql(:,:)    ! lattice coordinates
        ! tetrahedron integration method related data
        integer, allocatable :: kqid(:,:)   ! k-dependent weight of each q-point


    end type kq_set 

    type groundstate_type
    integer(4) :: lmaxapw
    end type groundstate_type

    type input_type
    type(groundstate_type) :: groundstate
    end type input_type

end module mod_minitypes