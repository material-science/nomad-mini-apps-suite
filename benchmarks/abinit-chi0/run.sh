#!/bin/bash

which sbatch &> /dev/null && { SLURM=true; SBATCH=sbatch; } || SLURM=false
which pjsub &> /dev/null && { PJM=true; SBATCH=pjsub; } || PJM=false

# Check environment variables or set default values
[ -z "${CHKP}" ] && export CHKP=ZrO2.bin
[ ! -f ${CHKP}  ] && { echo "ERROR Missing checkpoint file ${CHKP}"; exit -1; }
[ -z "${BUILD_DIR}" ] && export BUILD_DIR=../../build
[ ! -d "${BUILD_DIR}" ] && { echo "Cannot find build directory. BUILD_DIR=${BUILD_DIR}"; echo "Set the appropiate BUILD_DIR"; exit -1; }
export EXE=${BUILD_DIR}/bin/abinit-chi0
[ ! -f "${EXE}" ] && { echo "Could not find executable: ${EXE}"; echo "Make sure it is compiled"; exit -1; }
ldd ${EXE} | grep -q "not found" && { echo "Could not load executable: ${EXE}"; echo "Make sure to use the appropiate modules"; exit -1; }

# Get machine parameters: NCORES, ThrXcore, NSOCKETS & EXTRAQ
NCORES=0
source ../../machines
[ ${NCORES} -eq  0 ] && { echo "Need to config the number of cores in this machine"; exit -1; }

# Check if we are using Hyperthread
[ ${ThrXcore} -eq 1 ] && { HT=false; } || {
    { [ "${HT^^}" == "TRUE" ] || [ "${HT^^}" == "T" ] || [ "${HT}" == "1" ]; } && 
        { HT=true;  EXTRAQ+=$'\n'"#SBATCH --hint=multithread"; } ||
        { HT=false; EXTRAQ+=$'\n'"#SBATCH --hint=nomultithread"; }
}

# Set the number of task
${HT} && NTASK=$(( NCORES*ThrXcore )) || NTASK=${NCORES}

# Set the output file name
OUTPUT_FILE=OUTPUT.$( basename ${BUILD_DIR} | cut -c 7- )
${HT} && OUTPUT_FILE+="-ht"
[ ! -z "${PARTITION}" ] && OUTPUT_FILE+="-${PARTITION}"

${SLURM} && {
    grep -q -e power <<<${BUILD_DIR} && SRUN=false || SRUN=true
            # "mpirun -n ${NTASK} --bind-to core"    # If no hyperthread
            # mpirun -n ${NTASK} --bind-to hwthread # If Hyperthread
}
${PJM} && SRUN="mpirun -of \${LOG}"

# Create queue script header
${SLURM} &&
cat << EOF > que.sh
#!/bin/bash
#SBATCH --job-name=ABINIT-CHI0
#SBATCH --nodes=1
#SBATCH --output=${OUTPUT_FILE}
#SBATCH --time=00:30:00
#SBATCH --ntasks=${NTASK}
${EXTRAQ}
##SBATCH --cpus-per-task=1
##SBATCH --threads-per-core=1
##SBATCH --exclusive
##SBATCH --error=job.err
## ml ${LOADEDMODULES//:/ }
#export I_MPI_WAIT_MODE=1
$( grep -q karolina-gnu <<<${BUILD_DIR} && echo export OMPI_MCA_btl=^openib )
export OMP_NUM_THREADS=1
np=1
while true; do
    $( ${SRUN} &&
        echo "srun -n \${np} -c \$(( SLURM_NTASKS/np )) ${EXE} ${CHKP}" ||
        echo "mpirun -n \${np} --bind-to core --map-by socket:PE=\$(( SLURM_NPROCS/np )) ${EXE} ${CHKP}" )
    [ \${np} -eq ${NTASK} ] && break || np=\$(( np*2 ))
    [ \${np} -gt ${NTASK} ] && np=${NTASK}
done
EOF

${PJM} &&
cat << EOF > que.sh
#!/bin/bash
#PJM -N ABINIT-CHI0
#PJM -L node=1
#PJM -o ${OUTPUT_FILE}
#PJM -L elapse=00:30:00
#PJM -X
#PJM --mpi \"proc=${NTASK},max-proc-per-node=${NTASK}\"
${EXTRAQ}
##PJM -e job.err
export OMP_NUM_THREADS=1
${SRUN} ${EXE} ${CHKP}
EOF

echo "OUTPUT_FILE=${OUTPUT_FILE}"
${SBATCH} que.sh
[ ! -d BCK ] && mkdir BCK; cp que.sh BCK/que.$( basename ${BUILD_DIR} | cut -c 7- ).sh
