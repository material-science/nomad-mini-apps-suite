#!/bin/bash

source ../../machines

for mode in ${!modes[@]}; do
    module purge &> /dev/null
    module load ${modes[${mode}]} &> /dev/null || exit
    dir=build-${name}-${mode}
    for i in 1 2; do
        [ "$i" -eq 2 ] && dir=${dir}_vec
        export BUILD_DIR=../../${dir}
        [ ! -d ${BUILD_DIR} ] && { echo "BUILD_DIR=${BUILD_DIR} is missing"; continue; }
        ./run.sh
    done
done
