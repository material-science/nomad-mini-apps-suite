# exciting Mini-app Benchmark
[Link to main README](../../README.md)

## Running the benchmark
To run the benchmarks using the executable `../../build/bin/exciting-calcselfc` the next steps must be followed:

 
1. Make sure you have internet connexion available and get the necessary checkpoints by running the script:
    ```bash
    ./get-ckpts.sh
    ```
    Alternatively, manually download and extract them from:
    ```bash
    wget https://b2drop.bsc.es/index.php/s/mMo4nPMPi5ZaPs3/download/ckpts.tar.gz
    tar xvf ckpts.tar.gz
    ```
3. Check the `run.sh` script and if necessary modify it to fulfill your run preferences (locally or on a queue system such as SLURM). Alternatively, create your own script or run the executable directly by specifying the checkpoint to be used:

    ```bash
     ../../build/bin/exciting-calcselfc <checkpoint-file>
    ```
4. Run the benchmark and collect the results:
    ```bash
    ./run.sh
    ```
