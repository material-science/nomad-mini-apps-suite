#!/bin/bash

# Define the filenames
FILES="abinit-chi0/ZrO2.bin \
	fhiaims-lvl-tricoeff/Sitest.bin \
	fhiaims-lvl-tricoeff/ZrO2.bin \
        exciting-calcselfc/ZrO2_1.bin \
	exciting-calcselfc/ZrO2_2.bin"



# Check for missing files
missing=false
for f in $FILES; do [ ! -f "${f}" ] && missing=true; done
${missing} || exit 0

Addr="https://zenodo.org/records/10142416/files/NOMAD-miniapps-checkpoints.tgz?download=1"
baseAddr=$( echo ${Addr} | cut -d "/" -f 3 )

echo "Trying to download..."
# Test connection
timeout 2 nc -vz ${baseAddr} 443 &> /dev/null || {
    echo "ERROR Address not reachable"
    echo "Download and extract checkpoint files by hand from the benchmarks directory:"
    echo "    wget ${Addr}  -O - | tar -xz"
    exit 1
}

# Check if wget command is available
if ! command -v wget &> /dev/null; then
    if ! command -v curl &> /dev/null; then
        echo "Neither curl nor wget commands are available."
        echo "Download and extract checkpoint files by hand from the benchmarks directory:"
        echo "    wget ${Addr}"
        exit 1
    else
        # Download the archive
        curl -L ${Addr} | tar -xz
        exit 0
    fi
else
    # Download and extract
    wget ${Addr}  -O - | tar -xz
    exit 0
fi

