#!/bin/bash
source ./machines
[ -z "${RUN_CMAKE}" ] && RUN_CMAKE=false
for mode in ${!modes[@]}; do
    echo "[$mode] Loading modules: ${modes[${mode}]}"
    module purge &> /dev/null; module load ${modes[${mode}]} || exit
    [ "${mode}" == "nvhpc" ] && export FC=pgfortran
    [ "${mode}" == "fuji" ]  && export FC=frt && export FFLAGS=-SSL2BLAMP
    [ "${mode}" == "intel" ] && export FC=ifort # ifx is working poorly with complex numbers
    [ "${mode}" == "cray" ]  && export FC=ftn
    [ "${mode}" == "gnu" ]   && export FC=gfortran
    dir=build-${name}-${mode}
    for i in 1 2; do
        [ "$i" -eq 2 ] && { dir=${dir}_vec; EXT="-DOPTIMIZE_FOR_NATIVE=ON"; } || EXT=""
	[ -f ${CRAY_LIBSCI_PREFIX_DIR}/lib/libsci_${mode}_mp.so ] && EXT+=" -DBLAS_LIBRARIES=${CRAY_LIBSCI_PREFIX_DIR}/lib/libsci_${mode}_mp.so"
        [ -d ${dir} ] && { ${RUN_CMAKE} || [ ! -f ${dir}/Makefile ]; } && rm -rf ${dir}
        [ ! -d ${dir} ] && { cmake -B ${dir} ${EXT} || { echo "ERROR in cmake mode=${mode} dir=${dir}"; exit; } }
        make -C ${dir} $( [ ! -z "${VERBOSE}" ] && echo "VERBOSE=1" || echo "-j 64" ) || { echo "ERROR in cmake mode=${mode} dir=${dir}"; exit; }
    done
    unset FC FFLAGS
done
