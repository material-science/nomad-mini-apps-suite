module m_binfile

  private

  integer*4 :: fid=-1
  integer*4 :: endian
  logical :: bf_init=.false.
  public :: binfile_init, binfile_finalize
  public :: binfile_create, binfile_open
  public :: binfile_write, binfile_read
  public :: binfile_create_group
  public :: binfile_close_group
  public :: binfile_get_group
  public :: binfile_release_group

  interface binfile_write
    module procedure &
          binfile_write_str, &
          binfile_write_l0, &
          binfile_write_l1, &
          binfile_write_i0, &
          binfile_write_i1, &
          binfile_write_i2, &
          binfile_write_i3, &
          binfile_write_d0, &
          binfile_write_d1, &
          binfile_write_d2, &
          binfile_write_d3, &
          binfile_write_d6, &
          binfile_write_d7, &
          binfile_write_z0, &
          binfile_write_z1, &
          binfile_write_z2, &
          binfile_write_z3, &
          binfile_write_z4
  end interface binfile_write

  interface binfile_read
    module procedure &
          binfile_read_str, &
          binfile_read_l0, &
          binfile_read_l1, &
          binfile_read_i0, &
          binfile_read_i1, &
          binfile_read_i2, &
          binfile_read_i3, &
          binfile_read_d0, &
          binfile_read_d1, &
          binfile_read_d2, &
          binfile_read_d3, &
          binfile_read_d6, &
          binfile_read_d7, &
          binfile_read_z0, &
          binfile_read_z1, &
          binfile_read_z2, &
          binfile_read_z3, &
          binfile_read_z4
  end interface binfile_read

  type t_group
    integer*8 :: off, frst
    character*128 :: name
  end type t_group

  integer, parameter :: max_groups = 10
  integer :: group_id = 0
  type(t_group) :: g_list(max_groups)


  contains

  subroutine binfile_init()
  implicit none
  integer*1 v1(4)
  integer*4 v4
  equivalence(v1,v4)
  ! Test endianness
  v4=1
  if (v1(1)/=1) then
    write(*,*) 'm_binfile: This wrapper does not support BIG ENDIAN machines'
    STOP
  endif
  bf_init = .true.
  endian = v4
  end subroutine binfile_init

  subroutine binfile_finalize()
  implicit none
  if (fid/=-1) close(fid)
  fid=-1
  bf_init=.false.
  end subroutine binfile_finalize

  subroutine binfile_create(fname)
  implicit none
  character(len=*), intent(in) :: fname
  integer :: ios
  fid = get_file_unit( )
  open(unit=fid, file=trim(fname), access='stream', status='replace', &
      action='write', iostat=ios)
  call check( ios, 'open file '//trim(fname) )
  write(fid,iostat=ios) endian
  call check( ios, ' write endian' )
  end subroutine binfile_create

  subroutine binfile_open( fname )
  implicit none
  character(len=*), intent(in)  :: fname
  integer :: ios, f_endian
  fid = get_file_unit( )
  open(unit=fid, file=trim(fname), access='stream', status='old', &
      action='read', iostat=ios) !convert => LITTLE_ENDIAN/BIG_ENDIAN
  call check( ios, 'open file '//trim(fname) )
  read(fid) f_endian
  group_id = 1
  g_list(group_id)%frst = 1+sizeof(endian)
  end subroutine binfile_open


  subroutine binfile_write_str( name, d )
  implicit none
  character*(*), intent(in) :: name
  character*(*), intent(in) :: d
  integer :: l, n
  integer*8 :: jump
  l=len(trim(name))
  n=len(trim(d))
  jump = sizeof(n)+len(trim(d))
  write(fid) 'i0', l, trim(name), jump, n, trim(d)
  end subroutine binfile_write_str


  subroutine binfile_write_l0( name, d )
  implicit none
  character*(*), intent(in) :: name
  logical, intent(in) :: d
  integer*1 :: i_d
  integer :: l
  integer*8 :: jump
  i_d = merge(1,0,d)
  l=len(trim(name))
  jump = sizeof(i_d)
  write(fid) 'l0', l, trim(name), jump, i_d
  end subroutine binfile_write_l0



  subroutine binfile_write_l1( name, d, sz, lb )
  implicit none
  integer, parameter :: ndim=1
  character*(*), intent(in) :: name
  integer, intent(in) :: sz(ndim)
  logical, intent(in) :: d(sz(1))
  integer, optional :: lb(ndim)
  integer :: i, l, lbou(ndim)
  integer*1 :: i_d(sz(1))
  integer*8 :: jump
  if (present(lb)) then
    lbou = lb
  else
    lbou = 1
  endif
  l=len(trim(name))
  jump = 2*sizeof(lbou) + sizeof(d)
  forall(i=1:sz(1)) i_d(i) = merge(1,0,d(i))
  write(fid) 'l1', l, trim(name), jump, lbou(1), lbou(1)+size(d,1)-1, i_d
  end subroutine binfile_write_l1



  subroutine binfile_write_i0( name, d )
  implicit none
  character*(*), intent(in) :: name
  integer, intent(in) :: d
  integer :: l
  integer*8 :: jump
  l=len(trim(name))
  jump = sizeof(d)
  write(fid) 'i0', l, trim(name), jump, d
  end subroutine binfile_write_i0

  subroutine binfile_write_i1( name, d, sz, lb )
  implicit none
  integer, parameter :: ndim=1
  character*(*), intent(in) :: name
  integer, intent(in) :: sz(ndim)
  integer, intent(in) :: d(sz(1))
  integer, optional :: lb(ndim)
  integer :: l, lbou(ndim)
  integer*8 :: jump
  if (present(lb)) then
    lbou = lb
  else
    lbou = 1
  endif
  l=len(trim(name))
  jump = 2*sizeof(lbou) + sizeof(d)
  write(fid) 'i1', l, trim(name), jump, lbou(1), lbou(1)+size(d,1)-1, d
  end subroutine binfile_write_i1

  subroutine binfile_write_i2( name, d, sz, lb )
  implicit none
  integer, parameter :: ndim=2
  character*(*), intent(in) :: name
  integer, intent(in) :: sz(ndim)
  integer, intent(in) :: d(sz(1),sz(2))
  integer, optional :: lb(ndim)
  integer :: l, lbou(ndim)
  integer*8 :: jump
  if (present(lb)) then
    lbou = lb
  else
    lbou = 1
  endif
  l=len(trim(name))
  jump = 2*sizeof(lbou) + sizeof(d)
  write(fid) 'i2', l, trim(name), jump, &
      lbou(1), lbou(1)+size(d,1)-1, &
      lbou(2), lbou(2)+size(d,2)-1, &
      d
  end subroutine binfile_write_i2

  subroutine binfile_write_i3( name, d, sz, lb )
  implicit none
  integer, parameter :: ndim=3
  character*(*), intent(in) :: name
  integer, intent(in) :: sz(ndim)
  integer, intent(in) :: d(sz(1),sz(2),sz(3))
  integer, optional :: lb(ndim)
  integer :: l, lbou(ndim)
  integer*8 :: jump
  if (present(lb)) then
    lbou = lb
  else
    lbou = 1
  endif
  l=len(trim(name))
  jump = 2*sizeof(lbou) + sizeof(d)
  write(fid) 'i3', l, trim(name), jump, &
      lbou(1), lbou(1)+size(d,1)-1, &
      lbou(2), lbou(2)+size(d,2)-1, &
      lbou(3), lbou(3)+size(d,3)-1, &
      d
  end subroutine binfile_write_i3


  subroutine binfile_write_d0( name, d )
  implicit none
  character*(*), intent(in) :: name
  real*8, intent(in) :: d
  integer :: l
  integer*8 :: jump
  l=len(trim(name))
  jump = sizeof(d)
  write(fid) 'd0', l, trim(name), jump, d
  end subroutine binfile_write_d0

  subroutine binfile_write_d1( name, d, sz, lb )
  implicit none
  integer, parameter :: ndim=1
  character*(*), intent(in) :: name
  integer, intent(in) :: sz(ndim)
  real*8, intent(in) :: d(sz(1))
  integer, optional :: lb(ndim)
  integer :: l, lbou(ndim)
  integer*8 :: jump
  if (present(lb)) then
    lbou = lb
  else
    lbou = 1
  endif
  l=len(trim(name))
  jump = 2*sizeof(lbou) + sizeof(d)
  write(fid) 'd1', l, trim(name), jump, lbou(1), lbou(1)+size(d,1)-1, d
  end subroutine binfile_write_d1

  subroutine binfile_write_d2( name, d, sz, lb )
  implicit none
  integer, parameter :: ndim=2
  character*(*), intent(in) :: name
  integer, intent(in) :: sz(ndim)
  real*8, intent(in) :: d(sz(1),sz(2))
  integer, optional :: lb(ndim)
  integer :: l, lbou(ndim)
  integer*8 :: jump
  if (present(lb)) then
    lbou = lb
  else
    lbou = 1
  endif
  l=len(trim(name))
  jump = 2*sizeof(lbou) + sizeof(d)
  write(fid) 'd2', l, trim(name), jump, &
      lbou(1), lbou(1)+size(d,1)-1, &
      lbou(2), lbou(2)+size(d,2)-1, &
      d
  end subroutine binfile_write_d2

  subroutine binfile_write_d3( name, d, sz, lb )
  implicit none
  integer, parameter :: ndim=3
  character*(*), intent(in) :: name
  integer, intent(in) :: sz(ndim)
  real*8, intent(in) :: d(sz(1),sz(2),sz(3))
  integer, optional :: lb(ndim)
  integer :: l, lbou(ndim)
  integer*8 :: jump
  if (present(lb)) then
    lbou = lb
  else
    lbou = 1
  endif
  l=len(trim(name))
  jump = 2*sizeof(lbou) + sizeof(d)
  write(fid) 'd3', l, trim(name), jump, &
      lbou(1), lbou(1)+size(d,1)-1, &
      lbou(2), lbou(2)+size(d,2)-1, &
      lbou(3), lbou(3)+size(d,3)-1, &
      d
  end subroutine binfile_write_d3

  subroutine binfile_write_d6( name, d, sz, lb )
    implicit none
    integer, parameter :: ndim=6
    character*(*), intent(in) :: name
    integer, intent(in) :: sz(ndim)
    real*8, intent(in) :: d(sz(1),sz(2),sz(3),sz(4),sz(5),sz(6))
    integer, optional :: lb(ndim)
    integer :: l, lbou(ndim)
    integer*8 :: jump
    if (present(lb)) then
      lbou = lb
    else
      lbou = 1
    endif
    l=len(trim(name))
    jump = 2*sizeof(lbou) + sizeof(d)
    write(fid) 'd6', l, trim(name), jump, &
        lbou(1), lbou(1)+size(d,1)-1, &
        lbou(2), lbou(2)+size(d,2)-1, &
        lbou(3), lbou(3)+size(d,3)-1, &
        lbou(4), lbou(4)+size(d,4)-1, &
        lbou(5), lbou(5)+size(d,5)-1, &
        lbou(6), lbou(6)+size(d,6)-1, &
        d
  end subroutine binfile_write_d6

  subroutine binfile_write_d7( name, d, sz, lb )
    implicit none
    integer, parameter :: ndim=7
    character*(*), intent(in) :: name
    integer, intent(in) :: sz(ndim)
    real*8, intent(in) :: d(sz(1),sz(2),sz(3),sz(4),sz(5),sz(6),sz(7))
    integer, optional :: lb(ndim)
    integer :: l, lbou(ndim)
    integer*8 :: jump
    if (present(lb)) then
      lbou = lb
    else
      lbou = 1
    endif
    l=len(trim(name))
    jump = 2*sizeof(lbou) + sizeof(d)
    write(fid) 'd7', l, trim(name), jump, &
        lbou(1), lbou(1)+size(d,1)-1, &
        lbou(2), lbou(2)+size(d,2)-1, &
        lbou(3), lbou(3)+size(d,3)-1, &
        lbou(4), lbou(4)+size(d,4)-1, &
        lbou(5), lbou(5)+size(d,5)-1, &
        lbou(6), lbou(6)+size(d,6)-1, &
        lbou(7), lbou(7)+size(d,7)-1, &
        d
  end subroutine binfile_write_d7

  subroutine binfile_write_z0( name, d )
  implicit none
  character*(*), intent(in) :: name
  complex*16, intent(in) :: d
  integer :: l
  integer*8 :: jump
  l=len(trim(name))
  jump = sizeof(d)
  write(fid) 'z0', l, trim(name), jump, d
  end subroutine binfile_write_z0

  subroutine binfile_write_z1( name, d, sz, lb )
  implicit none
  integer, parameter :: ndim=1
  character*(*), intent(in) :: name
  integer, intent(in) :: sz(ndim)
  complex*16, intent(in) :: d(sz(1))
  integer, optional :: lb(ndim)
  integer :: l, lbou(ndim)
  integer*8 :: jump
  if (present(lb)) then
    lbou = lb
  else
    lbou = 1
  endif
  l=len(trim(name))
  jump = 2*sizeof(lbou) + sizeof(d)
  write(fid) 'd1', l, trim(name), jump, lbou(1), lbou(1)+size(d,1)-1, d
  end subroutine binfile_write_z1

  subroutine binfile_write_z2( name, d, sz, lb )
  implicit none
  integer, parameter :: ndim=2
  character*(*), intent(in) :: name
  integer, intent(in) :: sz(ndim)
  complex*16, intent(in) :: d(sz(1),sz(2))
  integer, optional :: lb(ndim)
  integer :: l, lbou(ndim)
  integer*8 :: jump
  if (present(lb)) then
    lbou = lb
  else
    lbou = 1
  endif
  l=len(trim(name))
  jump = 2*sizeof(lbou) + sizeof(d)
  write(fid) 'z2', l, trim(name), jump, &
      lbou(1), lbou(1)+size(d,1)-1, &
      lbou(2), lbou(2)+size(d,2)-1, &
      d
  end subroutine binfile_write_z2

  subroutine binfile_write_z3( name, d, sz, lb )
  implicit none
  integer, parameter :: ndim=3
  character*(*), intent(in) :: name
  integer, intent(in) :: sz(ndim)
  complex*16, intent(in) :: d(sz(1),sz(2),sz(3))
  integer, optional :: lb(ndim)
  integer :: l, lbou(ndim)
  integer*8 :: jump
  if (present(lb)) then
    lbou = lb
  else
    lbou = 1
  endif
  l=len(trim(name))
  jump = 2*sizeof(lbou) + sizeof(d)
  write(fid) 'z3', l, trim(name), jump, &
      lbou(1), lbou(1)+size(d,1)-1, &
      lbou(2), lbou(2)+size(d,2)-1, &
      lbou(3), lbou(3)+size(d,3)-1, &
      d
  end subroutine binfile_write_z3

  subroutine binfile_write_z4( name, d, sz, lb )
    implicit none
    integer, parameter :: ndim=4
    character*(*), intent(in) :: name
    integer, intent(in) :: sz(ndim)
    complex*16, intent(in) :: d(sz(1),sz(2),sz(3),sz(4))
    integer, optional :: lb(ndim)
    integer :: l, lbou(ndim)
    integer*8 :: jump
    if (present(lb)) then
      lbou = lb
    else
      lbou = 1
    endif
    l=len(trim(name))
    jump = 2*sizeof(lbou) + sizeof(d)
    write(fid) 'z4', l, trim(name), jump, &
        lbou(1), lbou(1)+size(d,1)-1, &
        lbou(2), lbou(2)+size(d,2)-1, &
        lbou(3), lbou(3)+size(d,3)-1, &
        lbou(4), lbou(4)+size(d,4)-1, &
        d
  end subroutine binfile_write_z4


  subroutine binfile_create_group( name )
  implicit none
  character*(*), intent(in) :: name
  integer*8 :: off
  integer :: l
  l=len(trim(name))
  write(fid) 'GR', l, trim(name)
  group_id = group_id + 1
  if (group_id>max_groups) stop 'MAX groups'
  inquire(unit=fid, pos=off )
  g_list(group_id)%off = off
  g_list(group_id)%name = trim(name)
  write(fid) 0_8
  end subroutine binfile_create_group

  subroutine binfile_close_group( name )
  implicit none
  character*(*), intent(in) :: name
  integer*8 :: off, poff
  inquire(unit=fid, pos=off )
  poff = g_list(group_id)%off
  write(fid,pos=poff) off-poff-sizeof(off)
  write(fid,pos=off)
  inquire(unit=fid, pos=off )
  group_id = group_id - 1
  end subroutine binfile_close_group

  subroutine binfile_get_group( name )
  implicit none
  character*(*), intent(in) :: name
  integer*8   :: off
  off = find_record( name, 'GR' )
  group_id = group_id+1
  g_list(group_id)%frst = off
  g_list(group_id)%name = trim(name)
  end subroutine binfile_get_group

  subroutine binfile_release_group( name )
  implicit none
  character*(*), intent(in) :: name
  character*2 :: t
  integer*8   :: off
  if (trim(name)/=trim(g_list(group_id)%name) ) then
    write(*,*) 'binfile_release_group names mismatch'
    write(*,*) '  ', trim(name)
    write(*,*) '  ', trim(trim(g_list(group_id)%name))
    stop
  endif
  group_id = group_id-1
  end subroutine binfile_release_group



  integer*8 function find_record( name, tag )
  implicit none
  character*(*), intent(in) :: name
  character*2, intent(in) :: tag
  integer*8 :: pos, jmp
  character*256 :: vname
  character*2 :: t
  integer :: l, ios
  logical :: found

  pos = g_list(group_id)%frst
  found = .false.
  do while (.not. found)
    read(fid,pos=pos,iostat=ios) t
    call check( ios, 'find_record('//trim(name)//')' )
    pos = pos+2
    read(fid,pos=pos) l
    pos=pos+4
    if (tag==t .and. l==len(trim(name))) then
      read(fid,pos=pos) vname(1:l)
      if (name(1:l)==vname(1:l)) found=.true.
    endif
    pos = pos+l
    if (found) then
      pos = pos+sizeof(jmp)
    else
      read(fid,pos=pos) jmp
      pos = pos+sizeof(jmp)+jmp
    endif
  enddo
  find_record = pos
  end function find_record


  subroutine binfile_read_str( name, d )
  implicit none
  character*(*), intent(in) :: name
  character*(*), intent(out) :: d
  character*64 :: str
  integer :: n
  integer*8 :: pos
  pos = find_record( name, 'i0' )
  read(fid,pos=pos) n
  read(fid) str(1:n)
  d = str(1:n)
  end subroutine binfile_read_str

  subroutine binfile_read_l0( name, d )
  implicit none
  character*(*), intent(in) :: name
  logical, intent(out) :: d
  integer*1 :: i_d
  integer :: l
  integer*8 :: pos
  pos = find_record( name, 'l0' )
  read(fid,pos=pos) i_d
  d = i_d==1
  end subroutine binfile_read_l0

  subroutine binfile_read_l1( name, d )
  implicit none
  integer, parameter :: ndim=1
  character*(*), intent(in) :: name
  logical, allocatable, intent(out) :: d(:)
  integer :: i, l, b(2,ndim)
  integer*1, allocatable :: i_d(:)
  integer*8 :: pos
  pos = find_record( name, 'l1' )
  read(fid,pos=pos) b
  allocate(d(b(1,1):b(2,1)))
  allocate(i_d(b(1,1):b(2,1)))
  read(fid) i_d
  FORALL(i=b(1,1):b(2,1)) d(i) = i_d(i)==1
  deallocate(i_d)
  end subroutine binfile_read_l1

  subroutine binfile_read_i0( name, d )
  implicit none
  character*(*), intent(in) :: name
  integer, intent(out) :: d
  integer :: l
  integer*8 :: pos
  pos = find_record( name, 'i0' )
  read(fid,pos=pos) d
  end subroutine binfile_read_i0

  subroutine binfile_read_i1( name, d )
  implicit none
  integer, parameter :: ndim=1
  character*(*), intent(in) :: name
  integer, allocatable, intent(out) :: d(:)
  integer :: l, b(2,ndim)
  integer*8 :: pos
  pos = find_record( name, 'i1' )
  read(fid,pos=pos) b
  allocate(d(b(1,1):b(2,1)))
  read(fid) d
  end subroutine binfile_read_i1

  subroutine binfile_read_i2( name, d )
  implicit none
  integer, parameter :: ndim=2
  character*(*), intent(in) :: name
  integer, allocatable, intent(out) :: d(:,:)
  integer :: l, b(2,ndim)
  integer*8 :: pos
  pos = find_record( name, 'i2' )
  read(fid,pos=pos) b
  allocate(d(b(1,1):b(2,1),b(1,2):b(2,2)))
  read(fid) d
  end subroutine binfile_read_i2

  subroutine binfile_read_i3( name, d )
  implicit none
  integer, parameter :: ndim=3
  character*(*), intent(in) :: name
  integer, allocatable, intent(out) :: d(:,:,:)
  integer :: l, b(2,ndim)
  integer*8 :: pos
  pos = find_record( name, 'i3' )
  read(fid,pos=pos) b
  allocate(d(b(1,1):b(2,1),b(1,2):b(2,2),b(1,3):b(2,3)))
  read(fid) d
  end subroutine binfile_read_i3


  subroutine binfile_read_d0( name, d )
  implicit none
  character*(*), intent(in) :: name
  real*8, intent(out) :: d
  integer :: l
  integer*8 :: pos
  pos = find_record( name, 'd0' )
  read(fid,pos=pos) d
  end subroutine binfile_read_d0

  subroutine binfile_read_d1( name, d )
  implicit none
  integer, parameter :: ndim=1
  character*(*), intent(in) :: name
  real*8, allocatable, intent(out) :: d(:)
  integer :: l, b(2,ndim)
  integer*8 :: pos
  pos = find_record( name, 'd1' )
  read(fid,pos=pos) b
  allocate(d(b(1,1):b(2,1)))
  read(fid) d
  end subroutine binfile_read_d1

  subroutine binfile_read_d2( name, d )
  implicit none
  integer, parameter :: ndim=2
  character*(*), intent(in) :: name
  real*8, allocatable, intent(out) :: d(:,:)
  integer :: l, b(2,ndim)
  integer*8 :: pos
  pos = find_record( name, 'd2' )
  read(fid,pos=pos) b
  allocate(d(b(1,1):b(2,1),b(1,2):b(2,2)))
  read(fid) d
  end subroutine binfile_read_d2

  subroutine binfile_read_d3( name, d )
  implicit none
  integer, parameter :: ndim=3
  character*(*), intent(in) :: name
  real*8, allocatable, intent(out) :: d(:,:,:)
  integer :: l, b(2,ndim)
  integer*8 :: pos
  pos = find_record( name, 'd3' )
  read(fid,pos=pos) b
  allocate(d(b(1,1):b(2,1),b(1,2):b(2,2),b(1,3):b(2,3)))
  read(fid) d
  end subroutine binfile_read_d3

  subroutine binfile_read_d6( name, d )
    implicit none
    integer, parameter :: ndim=6
    character*(*), intent(in) :: name
    real*8, allocatable, intent(out) :: d(:,:,:,:,:,:)
    integer :: l, b(2,ndim)
    integer*8 :: pos
    pos = find_record( name, 'd6' )
    read(fid,pos=pos) b
    allocate(d(b(1,1):b(2,1),b(1,2):b(2,2),b(1,3):b(2,3),b(1,4):b(2,4),b(1,5):b(2,5),b(1,6):b(2,6)))
    read(fid) d
  end subroutine binfile_read_d6

  subroutine binfile_read_d7( name, d )
    implicit none
    integer, parameter :: ndim=7
    character*(*), intent(in) :: name
    real*8, allocatable, intent(out) :: d(:,:,:,:,:,:,:)
    integer :: l, b(2,ndim)
    integer*8 :: pos
    pos = find_record( name, 'd7' )
    read(fid,pos=pos) b
    allocate(d(b(1,1):b(2,1),b(1,2):b(2,2),b(1,3):b(2,3),b(1,4):b(2,4),b(1,5):b(2,5),b(1,6):b(2,6),b(1,7):b(2,7)))
    read(fid) d
  end subroutine binfile_read_d7


  subroutine binfile_read_z0( name, d )
  implicit none
  character*(*), intent(in) :: name
  complex*16, intent(out) :: d
  integer :: l
  integer*8 :: pos
  pos = find_record( name, 'z0' )
  read(fid,pos=pos) d
  end subroutine binfile_read_z0

  subroutine binfile_read_z1( name, d )
  implicit none
  integer, parameter :: ndim=1
  character*(*), intent(in) :: name
  complex*16, allocatable, intent(out) :: d(:)
  integer :: l, b(2,ndim)
  integer*8 :: pos
  pos = find_record( name, 'z1' )
  read(fid,pos=pos) b
  allocate(d(b(1,1):b(2,1)))
  read(fid) d
  end subroutine binfile_read_z1

  subroutine binfile_read_z2( name, d )
  implicit none
  integer, parameter :: ndim=2
  character*(*), intent(in) :: name
  complex*16, allocatable, intent(out) :: d(:,:)
  integer :: l, b(2,ndim)
  integer*8 :: pos
  pos = find_record( name, 'z2' )
  read(fid,pos=pos) b
  allocate(d(b(1,1):b(2,1),b(1,2):b(2,2)))
  read(fid) d
  end subroutine binfile_read_z2

  subroutine binfile_read_z3( name, d )
  implicit none
  integer, parameter :: ndim=3
  character*(*), intent(in) :: name
  complex*16, allocatable, intent(out) :: d(:,:,:)
  integer :: l, b(2,ndim)
  integer*8 :: pos
  pos = find_record( name, 'z3' )
  read(fid,pos=pos) b
  allocate(d(b(1,1):b(2,1),b(1,2):b(2,2),b(1,3):b(2,3)))
  read(fid) d
  end subroutine binfile_read_z3

  subroutine binfile_read_z4( name, d )
    implicit none
    integer, parameter :: ndim=4
    character*(*), intent(in) :: name
    complex*16, allocatable, intent(out) :: d(:,:,:,:)
    integer :: l, b(2,ndim)
    integer*8 :: pos
    pos = find_record( name, 'z4' )
    read(fid,pos=pos) b
    allocate(d(b(1,1):b(2,1),b(1,2):b(2,2),b(1,3):b(2,3), b(1,4):b(2,4)))
    read(fid) d
    end subroutine binfile_read_z4





! get_file_unit returns a unit number that is not in use
  integer*4 function get_file_unit( )
    implicit none
    integer*4, parameter :: lu_max = 100
    integer*4 lu, m, iostat
    logical   opened

    do lu = lu_max, 1, -1
      inquire (unit=lu, opened=opened, iostat=iostat)
      if (iostat.ne.0) cycle
      if (.not.opened) exit
    end do
    get_file_unit = lu
    return
  end function get_file_unit

  subroutine check( ios, msg )
  implicit none
  integer :: ios
  character*(*) msg
  if (ios/=0) then
    write(*,*) 'ERROR: '//trim(msg)
    stop
  endif
  end subroutine 
end module m_binfile

subroutine write_checkpoint(mstart, mend, ik, iq, nstse)
  use m_binfile

  use modmain,                only: nspecies, natoms, idxas, idxlm, idxlo, &
  &                                 intgv, apword, nlorb, lorbl, &
  &                                 nlomax, apwordmax, nmatmax, spnstmax, natmtot
  use modgw,                  only: ibgw, nbgw, Gset, kqset, Gqset, Gkqset, Gqbarc, Gkset, freq, mbsiz, matsiz
  use mod_product_basis,      only: locmatsiz, mbindex, bradketa, bradketlo, mpwipw, &
  &                                 maxbigl, maxnmix, bradketc, locmixind, bigl, nmix
  use mod_coulomb_potential,  only: barc
  use mod_bands,              only: eveck, eveckp, eveckalm, eveckpalm
  use mod_misc_gw,            only: vi, atposl, Gamma
  use mod_gaunt_coefficients, only: gauntcoef
  use modinput,               only: input
  use mod_core_states,        only: corind, ncg
  use mod_selfenergy,         only: singc1, singc2, mwm
  use mod_dielectric_function, only: epsilon, epsh, epsw1, epsw2
  implicit none
  integer(4), intent(in)  :: mstart, mend, ik, iq, nstse
  call binfile_init()
  call binfile_create( "calcselfc_binckpt.out" )
  call binfile_write("/calcselfc_local_variables/mstart", mstart)
  call binfile_write("/calcselfc_local_variables/mend", mend)
  call binfile_write("/calcselfc_local_variables/ik", ik)
  call binfile_write("/calcselfc_local_variables/iq", iq)
  call binfile_write("/calcselfc_local_variables/nstse", nstse)

  call binfile_write("/modinput/input%groundstate%lmaxapw", input%groundstate%lmaxapw)

  call binfile_write("/modmain/nmatmax", nmatmax)
  call binfile_write("/modmain/nlomax", nlomax)
  call binfile_write("/modmain/natmtot", natmtot)
  call binfile_write("/modmain/apwordmax", apwordmax)
  call binfile_write("/modmain/spnstmax", spnstmax)
  call binfile_write("/modmain/idxlm", idxlm, shape(idxlm), lbound(idxlm))
  call binfile_write("/modmain/idxlo", idxlo, shape(idxlo), lbound(idxlo))
  call binfile_write("/modmain/apword", apword, shape(apword), lbound(apword))
  call binfile_write("/modmain/nlorb", nlorb, shape(nlorb), lbound(nlorb))
  call binfile_write("/modmain/lorbl", lorbl, shape(lorbl), lbound(lorbl))

  call binfile_write("/modgw/ibgw", ibgw)
  call binfile_write("/modgw/nbgw", nbgw)
  call binfile_write("/modgw/freq%nomeg", freq%nomeg)
  call binfile_write("/modgw/Gset%intgv", Gset%intgv, shape(Gset%intgv), lbound(Gset%intgv))
  call binfile_write("/modgw/Gset%ivg", Gset%ivg, shape(Gset%ivg), lbound(Gset%ivg))
  call binfile_write("/modgw/Gset%ivgig", Gset%ivgig, shape(Gset%ivgig), lbound(Gset%ivgig))
  call binfile_write("/modgw/Gqset%ngk", Gqset%ngk, shape(Gqset%ngk), lbound(Gqset%ngk))
  call binfile_write("/modgw/Gqset%igigk", Gqset%igigk, shape(Gqset%igigk), lbound(Gqset%igigk))
  call binfile_write("/modgw/Gqset%igkig", Gqset%igkig, shape(Gqset%igkig), lbound(Gqset%igkig))
  call binfile_write("/modgw/Gkqset%ngk", Gkqset%ngk, shape(Gkqset%ngk), lbound(Gkqset%ngk))
  call binfile_write("/modgw/Gkqset%igigk", Gkqset%igigk, shape(Gkqset%igigk), lbound(Gkqset%igigk))
  call binfile_write("/modgw/Gkqset%igkig", Gkqset%igkig, shape(Gkqset%igkig), lbound(Gkqset%igkig))
  call binfile_write("/modgw/kqset%nkpt", kqset%nkpt)
  call binfile_write("/modgw/kqset%kqid", kqset%kqid, shape(kqset%kqid), lbound(kqset%kqid))
  call binfile_write("/modgw/kqset%vkl", kqset%vkl, shape(kqset%vkl), lbound(kqset%vkl))
  call binfile_write("/modgw/kqset%vql", kqset%vql, shape(kqset%vql), lbound(kqset%vql))
  call binfile_write("/modgw/Gqbarc%ngk", Gqbarc%ngk, shape(Gqbarc%ngk), lbound(Gqbarc%ngk))
  call binfile_write("/modgw/Gqbarc%igigk", Gqbarc%igigk, shape(Gqbarc%igigk), lbound(Gqbarc%igigk))
  call binfile_write("/modgw/Gqbarc%igkig", Gqbarc%igkig, shape(Gqbarc%igkig), lbound(Gqbarc%igkig))
  call binfile_write("/modgw/Gkset%ngk", Gkset%ngk, shape(Gkset%ngk), lbound(Gkset%ngk))
  call binfile_write("/modgw/Gkset%igigk", Gkset%igigk, shape(Gkset%igigk), lbound(Gkset%igigk))
  call binfile_write("/modgw/Gkset%igkig", Gkset%igkig, shape(Gkset%igkig), lbound(Gkset%igkig))

  call binfile_write("/mod_product_basis/mbsiz", mbsiz)
  call binfile_write("/mod_product_basis/locmatsiz", locmatsiz)
  call binfile_write("/mod_product_basis/matsiz", matsiz)
  call binfile_write("/mod_product_basis/maxbigl", maxbigl)
  call binfile_write("/mod_product_basis/maxnmix", maxnmix)
  call binfile_write("/mod_product_basis/mbindex", mbindex, shape(mbindex), lbound(mbindex))
  call binfile_write("/mod_product_basis/bradketa", bradketa, shape(bradketa), lbound(bradketa))
  call binfile_write("/mod_product_basis/bradketlo", bradketlo, shape(bradketlo), lbound(bradketlo))
  call binfile_write("/mod_product_basis/bradketc", bradketc, shape(bradketc), lbound(bradketc))
  call binfile_write("/mod_product_basis/mpwipw", mpwipw, shape(mpwipw), lbound(mpwipw))
  call binfile_write("/mod_product_basis/locmixind", locmixind, shape(locmixind), lbound(locmixind))
  call binfile_write("/mod_product_basis/bigl", bigl, shape(bigl), lbound(bigl))
  call binfile_write("/mod_product_basis/nmix", nmix, shape(nmix), lbound(nmix))

  call binfile_write("/mod_coulomb_potential/barc", barc, shape(barc), lbound(barc))
  call binfile_write("/mod_bands/eveck", eveck, shape(eveck), lbound(eveck))
  call binfile_write("/mod_bands/eveckp", eveckp, shape(eveckp), lbound(eveckp))
  call binfile_write("/mod_bands/eveckpalm", eveckpalm, shape(eveckpalm), lbound(eveckpalm))
  call binfile_write("/mod_bands/eveckalm", eveckalm, shape(eveckalm), lbound(eveckalm))
  call binfile_write("/mod_atoms/idxas", idxas, shape(idxas), lbound(idxas))
  call binfile_write("/mod_misc_gw/Gamma", Gamma)

  call binfile_write("/mod_misc_gw/vi", vi)
  call binfile_write("/mod_misc_gw/atposl", atposl, shape(atposl), lbound(atposl))
  call binfile_write("/mod_gaunt_coefficients/gauntcoef", gauntcoef, shape(gauntcoef), lbound(gauntcoef))
  call binfile_write("/mod_core_states/corind", corind, shape(corind), lbound(corind))
  call binfile_write("/mod_core_states/ncg", ncg)
  call binfile_write("/mod_dielectric_function/epsilon", epsilon, shape(epsilon), lbound(epsilon))
  call binfile_write("/mod_dielectric_function/epsh", epsh, shape(epsh), lbound(epsh))
  call binfile_write("/mod_dielectric_function/epsw1", epsw1, shape(epsw1), lbound(epsw1))
  call binfile_write("/mod_dielectric_function/epsw2", epsw2, shape(epsw2), lbound(epsw2))
  call binfile_write("/mod_selfenergy/singc1", singc1)
  call binfile_write("/mod_selfenergy/singc2", singc2)
  call binfile_write("/mod_selfenergy/mwm", mwm, shape(mwm), lbound(mwm))

end subroutine write_checkpoint

subroutine calcselfc(iq)
    use modinput
    use modmain,    only : nstfv, apwordmax, lmmaxapw, natmtot, nspnfv, &
    &                      zzero, nmatmax
    use modgw
    use mod_mpi_gw, only : myrank
    use m_getunit
    use m_binfile
    implicit none
    ! input/output
    integer(4), intent(in) :: iq
    ! local
    integer(4) :: ik, ikp, jk, ispn
    integer(4) :: mdim, iblk, nblk, mstart, mend
    integer(4) :: fid
    real(8) :: tstart, tend, t0, t1

    call timesec(tstart)

    !------------------------
    ! total number of states
    !------------------------
    if (input%gw%coreflag=='all') then
      mdim = nstse+ncg
    else
      mdim = nstse
    end if

    !-------------------------------------------------------
    ! determine the number of blocks used in minm operation
    !-------------------------------------------------------
    if (mblksiz >= mdim) then
      nblk = 1
    else
      nblk = mdim / mblksiz
      if (mod(mdim,mblksiz) /= 0) nblk = nblk+1
    end if

    !-------------------------------------------
    ! products M*W^c*M
    !-------------------------------------------
    allocate(mwm(ibgw:nbgw,1:mdim,1:freq%nomeg))
    ! msize = sizeof(mwm)*b2mb
    ! write(*,'(" calcselfc: size(mwm) (Mb):",f12.2)') msize

    allocate(eveckalm(nstfv,apwordmax,lmmaxapw,natmtot))
    allocate(eveckpalm(nstfv,apwordmax,lmmaxapw,natmtot))
    allocate(eveck(nmatmax,nstfv))
    allocate(eveckp(nmatmax,nstfv))

    !================================
    ! loop over irreducible k-points
    !================================
    ! write(*,*)
    do ikp = 1, kset%nkpt
      ! write(*,*) 'calcselfc: rank, (iq, ikp):', myrank, iq, ikp

      ! k vector
      ik = kset%ikp2ik(ikp)
      ! k-q vector
      jk = kqset%kqid(ik,iq)

      ! get KS eigenvectors
      call get_evec_gw(kqset%vkl(:,jk), Gkqset%vgkl(:,:,:,jk), eveck)
      eveckp = conjg(eveck)
      call get_evec_gw(kqset%vkl(:,ik), Gkqset%vgkl(:,:,:,ik), eveck)

      call expand_evec(ik, 't')
      call expand_evec(jk, 'c')

      !=================================
      ! Loop over m-blocks in M^i_{nm}
      !=================================
      !This loop is the main computational kernel in the case ZrO2_1

      do iblk = 1, nblk

        mstart = 1 + (iblk-1)*mblksiz
        mend = min(mdim, mstart+mblksiz-1)

        if((myrank == 0).and.(iblk == 1).and.(ikp == 1).and.(iq == 1)) then
          ! call write_checkpoint(mstart, mend, ik, iq, nstse, mdim)
          call write_checkpoint(mstart, mend, ik, iq, nstse)
        endif

        ! m-block M^i_{nm}
        allocate(minmmat(mbsiz,ibgw:nbgw,mstart:mend))
        msize = sizeof(mwm)*b2mb
        write(*,'(" calcselfc: rank, size(mwm) (Mb):",3i4,f12.2)') myrank, mstart, mend, msize

        call expand_products(ik, iq, ibgw, nbgw, -1, mstart, mend, nstse, minmmat)

        !================================================================
        ! Calculate weight(q)*Sum_ij{M^i*W^c_{ij}(k,q;\omega)*conjg(M^j)}
        !================================================================
        call calcmwm(ibgw, nbgw, mstart, mend, minmmat)

        deallocate(minmmat)

        if((myrank == 0).and.(iblk == 1).and.(ikp == 1).and.(iq == 1)) then
          write(*,*) "Writing result"
          call binfile_write("/mod_selfenergy/mwm_result", mwm, shape(mwm), lbound(mwm))
          call binfile_finalize()
        endif

      end do ! iblk

      !=======================================
      ! Calculate the correlation self-energy
      !=======================================
      if (input%gw%taskname=='cohsex') then
        call calcselfc_cohsex(ikp, iq, mdim)
      else
        if (input%gw%selfenergy%method == 'cd') then
          ! Contour deformation technique
          call calcselfc_freqconv_cd(ikp, iq, mdim)
        else if (input%gw%selfenergy%method == 'ac') then
          ! Imaginary frequency formalism
          call calcselfc_freqconv_ac(ikp, iq, mdim)
        end if
      end if

    end do ! ikp

    deallocate(eveck)
    deallocate(eveckp)
    deallocate(eveckalm)
    deallocate(eveckpalm)

    ! delete MWM
    deallocate(mwm)

    ! timing
    call timesec(tend)
    time_selfc = time_selfc+tend-tstart

    return
    
end subroutine


