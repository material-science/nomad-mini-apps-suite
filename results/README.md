# NOMAD Mini-apps Suite Results
This folder is intended to document all the performance results obtainted from running the experiments in the benchmarks folder. It is encouraged to commit the results obtained on different machines to this repository.

#TODO 1: Agree on a standard set of variables to identify each experiment and performance metrics relevant to the co-design activity.

#TODO 2: Create an updatable file to submit and document all the experiments as they are done (excel?, csv?, etc.)

#TODO 3: Create a Python script or scripts to read the experiments, compare and visualize the results with the objective of providing insightful information.